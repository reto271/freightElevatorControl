import pickle
from enum import Enum

class ProtocolVersion(Enum):
    ProtocolVersion_1 = 0 + 100  # Prevent from transmitting 0


class SimulationMsg():

    class EMsg(Enum):
        # Control Messages
        Boot = 0
        TerminateTest = 1

        # Inputs to the Elevator Control
        ButtonUpPressed = 2
        ButtonDownPressed = 3
        ShaftSignalTop = 4
        ShaftSignalBottom = 5

        # Outputs from the Elevator Control
        LightBarrier = 6
        MoveUpRelais = 7
        MoveDownRelais = 8


    def __init__(self, msgId, msgData):
        self.m_msgId = msgId
        self.m_data = msgData

    def getId(self):
        return self.m_msgId

    def getData(self):
        return self.m_data



def serializeAndSend(socket, msg):
    socket.sendall(pickle.dumps(msg))


def deserializeMsg(data):
    return pickle.loads(data)


def printMsg(simMsg, logger):
    logger.logText('type: ' + str(type(simMsg)) +
                   ', id: ' + str(simMsg.getId()) +
                   ', data: ' + str(simMsg.getData()))
