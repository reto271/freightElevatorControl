#!/usr/bin/python3


import threading
import abc


class TaskBase (threading.Thread):
    def __init__(self, taskID, taskName, debugLogger):
        threading.Thread.__init__(self)
        self.m_taskID = taskID
        self.m_taskName = taskName
        self.m_logger = debugLogger

    @abc.abstractmethod
    def run(self):
        pass
