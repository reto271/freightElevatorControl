#!/usr/bin/python3

from enum import Enum

class MoveDirection(Enum):
    Unknown = 0
    Up = 1
    Down = 2


class Command(Enum):
    Down = 0
    Up = 1
    Stop = 2
