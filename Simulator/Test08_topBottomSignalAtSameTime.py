#!/usr/bin/python3
# encoding: utf-8

import sys
sys.path.append('./Common')

from TestBase import TestBase


## Test 08
#
# Top and bottom signal active at the same time while moving. Expect elevator controller to stop.
class Test08_topBottomSignalAtSameTime(TestBase):

    ## Constructor, requires a reference to the IIOHandler
    def __init__(self, threadId, threadName, msgQueueSize, tcpSocket, debugLogger):
        super(Test08_topBottomSignalAtSameTime, self).__init__(threadId, threadName, msgQueueSize, tcpSocket, debugLogger)
        self.m_logger = debugLogger

    ## Test function
    def run(self):
        self.m_isRunning = True

        #Initialize at middle
        # def setIntialStates(self, buttonUp, buttonDown, shaftTop, shaftBottom):
        self.setIntialStates(0, 0, 0, 0)

        self.wait(1000)
        self.expect(TestBase.CtrlOutput.LightBarrier, 0)
        self.expect(TestBase.CtrlOutput.MoveDownRelais, 0)
        self.expect(TestBase.CtrlOutput.MoveUpRelais, 0)

        self.pressButton(TestBase.Button.Down)
        self.checkMoveStartAndSimStartShaftTransitions(TestBase.Direction.Down)

        self.wait(3000)
        # Will assert if the shaft signals change in the other order. The move controller asserts if
        #  the car is moving down and top is reached.
        self.setShaftSignal(TestBase.ShaftPos.Bottom, 1)
        self.setShaftSignal(TestBase.ShaftPos.Top, 1)

        # The car stops immediately
        self.wait(300)
        self.checkMoveStop()

        # Terminate the test
        self.m_isRunning = False
        self.sendTestEnd()
