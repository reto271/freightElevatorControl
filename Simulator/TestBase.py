#!/usr/bin/python3
# encoding: utf-8

import sys
sys.path.append('./Common')

import time
import inspect

from enum import Enum

import SimulatorMsg
from SimulatorMsg import SimulationMsg

from TaskBase import TaskBase


## Test Base
#
# Base class with test utils. Functions to send debug states and functions
# to check expectations.
class TestBase(TaskBase):

    class CtrlOutput(Enum):
        LightBarrier = 0
        MoveUpRelais = 1
        MoveDownRelais = 2

    class Button(Enum):
        Down = 0
        Up = 1

    class Direction(Enum):
        Down = 0
        Up = 1

    class ShaftPos(Enum):
        Top = 0
        Bottom = 1
        Middle = 2

    ## Constructor
    def __init__(self, threadId, threadName, msgQueueSize, tcpSocket, debugLogger):
        super(TestBase, self).__init__(threadId, threadName, debugLogger)
        self.m_logger = debugLogger
        self.m_isRunning = False
        self.m_tcpSocket = tcpSocket
        self.m_lightBarrier = 0
        self.m_moveUpRelais = 0
        self.m_moveDownRelais = 0
        self.m_feedback = True
        self.m_errorStrings = ''


    def setup(self, dummy):
        pass


    def isRunning(self):
        return self.m_isRunning


    def getSocket(self):
        return self.m_tcpSocket


    def getLogger(self):
        return self.m_logger


    def putSimulatorMsg(self, simMsg):
        if(SimulationMsg.EMsg.LightBarrier == simMsg.getId()):
            self.m_lightBarrier = simMsg.getData()
        elif(SimulationMsg.EMsg.MoveUpRelais == simMsg.getId()):
            self.m_moveUpRelais = simMsg.getData()
        elif(SimulationMsg.EMsg.MoveDownRelais == simMsg.getId()):
            self.m_moveDownRelais = simMsg.getData()
        else:
            assert False


    def setIntialStates(self, buttonUp, buttonDown, shaftTop, shaftBottom):
        self.m_logger.logText('Send initial states')
        self.sendInputMsg(SimulationMsg.EMsg.ButtonUpPressed, buttonUp)
        time.sleep(0.3)
        self.sendInputMsg(SimulationMsg.EMsg.ButtonDownPressed, buttonDown)
        time.sleep(0.3)
        self.sendInputMsg(SimulationMsg.EMsg.ShaftSignalTop, shaftTop)
        time.sleep(0.3)
        self.sendInputMsg(SimulationMsg.EMsg.ShaftSignalBottom, shaftBottom)
        time.sleep(0.3)


    def sendInputMsg(self, msgId, value):
        SimulatorMsg.serializeAndSend(self.m_tcpSocket, SimulationMsg(msgId, value))
        time.sleep(0.01) # To make sure each frame is sent separately


    def sendTestEnd(self):
        SimulatorMsg.serializeAndSend(self.m_tcpSocket, SimulationMsg(SimulationMsg.EMsg.TerminateTest, 0))


    def wait(self, ms_timeout):
        time.sleep(ms_timeout / 1000)


    def pressButton(self, button):
        if (TestBase.Button.Down == button):
            self.sendInputMsg(SimulationMsg.EMsg.ButtonDownPressed, 1)
            self.wait(100)
            self.sendInputMsg(SimulationMsg.EMsg.ButtonDownPressed, 0)
        elif (TestBase.Button.Up == button):
            self.sendInputMsg(SimulationMsg.EMsg.ButtonUpPressed, 1)
            self.wait(100)
            self.sendInputMsg(SimulationMsg.EMsg.ButtonUpPressed, 0)
        else:
            assert False


    def expect(self, outputSignal, expectedState):
        feedback = self.__expect(outputSignal, expectedState)
        if (False == feedback[0]):
            self.m_logger.logText('Expectation not met')
            self.m_logger.logText('    ' + inspect.stack()[1][1]+
                                  ', ln: ' + str(inspect.stack()[1][2]) +
                                  ', fnc: ' + inspect.stack()[1][3] +
                                  ', val: ' + str(outputSignal))
            self.m_logger.logText('    expected value: ' + str(expectedState))
            self.m_logger.logText('    actual value: ' + str(feedback[1]))

            self.__addErrorLine(inspect.stack()[1][1] +
                                ', ln: ' + str(inspect.stack()[1][2]) +
                                ', fnc: ' + inspect.stack()[1][3] +
                                ', val: ' + str(outputSignal) +
                                ', expected: ' + str(expectedState) +
                                ', actual: ' + str(feedback[1]))

    def getErrorString(self):
        return self.m_errorStrings

    def __addErrorLine(self, errorText):
        self.m_errorStrings = self.m_errorStrings + errorText + '\n'


    def __expect(self, outputSignal, expectedState):
        if (TestBase.CtrlOutput.LightBarrier == outputSignal):
            feedback = (self.m_lightBarrier == expectedState)
            self.m_feedback = self.m_feedback and feedback
            return (feedback, self.m_lightBarrier)
        elif (TestBase.CtrlOutput.MoveUpRelais == outputSignal):
            feedback =  (self.m_moveUpRelais == expectedState)
            self.m_feedback = self.m_feedback and feedback
            return (feedback, self.m_moveUpRelais)
        elif (TestBase.CtrlOutput.MoveDownRelais == outputSignal):
            feedback = (self.m_moveDownRelais == expectedState)
            self.m_feedback = self.m_feedback and feedback
            return (feedback, self.m_moveDownRelais)
        else:
            self.m_feedback = (False, False)
            assert False


    def setShaftSignal(self, shaftPosition, state):
        if (TestBase.ShaftPos.Top == shaftPosition):
            self.sendInputMsg(SimulationMsg.EMsg.ShaftSignalTop, state)
        elif (TestBase.ShaftPos.Bottom == shaftPosition):
            self.sendInputMsg(SimulationMsg.EMsg.ShaftSignalBottom, state)
        else:
            assert False


    def getFeedback(self):
        return self.m_feedback


    # Check move start and simulate minimal shaft transitions
    def checkMoveStartAndSimStartShaftTransitions(self, direction):
        # Switch on light barrier
        self.wait(50)
        self.expect(TestBase.CtrlOutput.LightBarrier, 1)
        self.wait(40)
        # Simulate leaving shaft end
        self.setShaftSignal(TestBase.ShaftPos.Top, 0)
        self.setShaftSignal(TestBase.ShaftPos.Bottom, 0)
        # Check move relais
        if (TestBase.Direction.Down == direction):
            self.expect(TestBase.CtrlOutput.MoveDownRelais, 1)
            self.expect(TestBase.CtrlOutput.MoveUpRelais, 0)
        elif (TestBase.Direction.Up == direction):
            self.expect(TestBase.CtrlOutput.MoveDownRelais, 0)
            self.expect(TestBase.CtrlOutput.MoveUpRelais, 1)
        else:
            assert(False)

    # Check that the move is properly stopped
    def checkMoveStop(self):
        self.wait(500)
        self.expect(TestBase.CtrlOutput.LightBarrier, 0)
        self.expect(TestBase.CtrlOutput.MoveDownRelais, 0)
        self.expect(TestBase.CtrlOutput.MoveUpRelais, 0)
