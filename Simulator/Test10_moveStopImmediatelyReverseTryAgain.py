#!/usr/bin/python3
# encoding: utf-8

import sys
sys.path.append('./Common')

from TestBase import TestBase


## Test 10
#
# Move from top to bottom, reverse immediately. Reversing is rejected. Try again
#  reversing then accepted
class Test10_moveStopImmediatelyReverseTryAgain(TestBase):

    ## Constructor, requires a reference to the IIOHandler
    def __init__(self, threadId, threadName, msgQueueSize, tcpSocket, debugLogger):
        super(Test10_moveStopImmediatelyReverseTryAgain, self).__init__(threadId, threadName, msgQueueSize, tcpSocket, debugLogger)
        self.m_logger = debugLogger

    ## Test function
    def run(self):
        self.m_isRunning = True

        #Initialize at top
        # def setIntialStates(self, buttonUp, buttonDown, shaftTop, shaftBottom):
        self.setIntialStates(0, 0, 1, 0)

        self.wait(1000)
        self.expect(TestBase.CtrlOutput.LightBarrier, 0)
        self.expect(TestBase.CtrlOutput.MoveDownRelais, 0)
        self.expect(TestBase.CtrlOutput.MoveUpRelais, 0)

        self.pressButton(TestBase.Button.Down)
        self.checkMoveStartAndSimStartShaftTransitions(TestBase.Direction.Down)

        self.wait(3000)

        # Stop the car
        self.pressButton(TestBase.Button.Up)
        self.checkMoveStop()

        # No delay start move in opposite direction immediately
        self.m_logger.logText('----- Reverse immediately -----')
        self.pressButton(TestBase.Button.Up)

        # The move does not start, the controller rejects it
        for x in range(10):
            self.expect(TestBase.CtrlOutput.LightBarrier, 0)
            self.expect(TestBase.CtrlOutput.MoveDownRelais, 0)
            self.expect(TestBase.CtrlOutput.MoveUpRelais, 0)
            self.wait(50)


        #Restart move in other direction again after relaxation time
        self.wait(2000)
        self.m_logger.logText('----- Start move other direction -----')
        self.pressButton(TestBase.Button.Up)
        self.checkMoveStartAndSimStartShaftTransitions(TestBase.Direction.Up)

        self.wait(2000)
        self.m_logger.logText('----- Top reached -----')
        self.setShaftSignal(TestBase.ShaftPos.Top, 1)
        self.checkMoveStop()

        # Terminate the test
        self.m_isRunning = False
        self.sendTestEnd()
