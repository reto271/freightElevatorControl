#!/usr/bin/python3
# encoding: utf-8

import sys
sys.path.append('./Common')

from TestBase import TestBase


## Test 04
#
# Move from top to bottom
class Test04_moveDownStopMoveUp(TestBase):

    ## Constructor, requires a reference to the IIOHandler
    def __init__(self, threadId, threadName, msgQueueSize, tcpSocket, debugLogger):
        super(Test04_moveDownStopMoveUp, self).__init__(threadId, threadName, msgQueueSize, tcpSocket, debugLogger)
        self.m_logger = debugLogger

    ## Test function
    def run(self):
        self.m_isRunning = True

        # def setIntialStates(self, buttonUp, buttonDown, shaftTop, shaftBottom):
        self.setIntialStates(0, 0, 0, 0)

        self.wait(1000)
        self.expect(TestBase.CtrlOutput.LightBarrier, 0)
        self.expect(TestBase.CtrlOutput.MoveDownRelais, 0)
        self.expect(TestBase.CtrlOutput.MoveUpRelais, 0)

        self.pressButton(TestBase.Button.Down)
        self.wait(50)
        self.expect(TestBase.CtrlOutput.LightBarrier, 1)
        self.wait(50)
        self.setShaftSignal(TestBase.ShaftPos.Top, 1)
        self.wait(150)
        self.expect(TestBase.CtrlOutput.MoveDownRelais, 1)
        self.expect(TestBase.CtrlOutput.MoveUpRelais, 0)
        self.wait(100)
        self.setShaftSignal(TestBase.ShaftPos.Top, 0)

        self.wait(2000)

        # Stop the car
        self.pressButton(TestBase.Button.Up)
        self.wait(300)
        self.expect(TestBase.CtrlOutput.LightBarrier, 0)
        self.expect(TestBase.CtrlOutput.MoveDownRelais, 0)
        self.expect(TestBase.CtrlOutput.MoveUpRelais, 0)

        self.wait(2500) # timeout for the relais is 2s

        #Restart move in other direction
        self.m_logger.logText('----- Start move other direction -----')
        self.pressButton(TestBase.Button.Up)
        self.wait(50)
        self.expect(TestBase.CtrlOutput.LightBarrier, 1)
        self.wait(200)
        self.expect(TestBase.CtrlOutput.LightBarrier, 1)
        self.expect(TestBase.CtrlOutput.MoveDownRelais, 0)
        self.expect(TestBase.CtrlOutput.MoveUpRelais, 1)

        self.m_logger.logText('----- Top reached -----')
        self.wait(2000)
        self.setShaftSignal(TestBase.ShaftPos.Top, 1)
        self.wait(200)
        self.expect(TestBase.CtrlOutput.MoveDownRelais, 0)
        self.expect(TestBase.CtrlOutput.MoveUpRelais, 0)
        self.wait(200)
        self.expect(TestBase.CtrlOutput.LightBarrier, 0)

        # Terminate the test
        self.m_isRunning = False
        self.sendTestEnd()
