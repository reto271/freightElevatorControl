#!/usr/bin/python3
# encoding: utf-8

import sys
sys.path.append('./Common')

from TestBase import TestBase


## Test 12
#
# At bottom, move request downwards. Will be rejected.
class Test12_atBottomMoveDownRequest(TestBase):

    ## Constructor, requires a reference to the IIOHandler
    def __init__(self, threadId, threadName, msgQueueSize, tcpSocket, debugLogger):
        super(Test12_atBottomMoveDownRequest, self).__init__(threadId, threadName, msgQueueSize, tcpSocket, debugLogger)
        self.m_logger = debugLogger

    ## Test function
    def run(self):
        self.m_isRunning = True

        #Initialize at top
        # def setIntialStates(self, buttonUp, buttonDown, shaftTop, shaftBottom):
        self.setIntialStates(0, 0, 0, 1)

        self.wait(1000)
        self.expect(TestBase.CtrlOutput.LightBarrier, 0)
        self.expect(TestBase.CtrlOutput.MoveDownRelais, 0)
        self.expect(TestBase.CtrlOutput.MoveUpRelais, 0)

        # Try to move down if already at bottom, move shall be rejected
        self.pressButton(TestBase.Button.Down)
        for x in range(20):
            self.expect(TestBase.CtrlOutput.MoveDownRelais, 0)
            self.expect(TestBase.CtrlOutput.MoveUpRelais, 0)
            self.wait(20)
        # The light barrier might be on for a short time, therefore only check at the end of the test
        self.expect(TestBase.CtrlOutput.LightBarrier, 0)

        # Terminate the test
        self.m_isRunning = False
        self.sendTestEnd()
