#!/usr/bin/python3
# encoding: utf-8

import sys
sys.path.append('./Common')

from TestBase import TestBase


## Test 01
#
# Move from top to bottom
class Test01_moveTopBottom(TestBase):

    ## Constructor, requires a reference to the IIOHandler
    def __init__(self, threadId, threadName, msgQueueSize, tcpSocket, debugLogger):
        super(Test01_moveTopBottom, self).__init__(threadId, threadName, msgQueueSize, tcpSocket, debugLogger)
        self.m_logger = debugLogger

    ## Test function
    def run(self):
        self.m_isRunning = True

        #Initialize at top
        # def setIntialStates(self, buttonUp, buttonDown, shaftTop, shaftBottom):
        self.setIntialStates(0, 0, 1, 0)

        self.wait(1000)
        self.expect(TestBase.CtrlOutput.LightBarrier, 0)
        self.expect(TestBase.CtrlOutput.MoveDownRelais, 0)
        self.expect(TestBase.CtrlOutput.MoveUpRelais, 0)

        self.pressButton(TestBase.Button.Down)
        self.wait(100)
        self.expect(TestBase.CtrlOutput.LightBarrier, 1)
        self.wait(200)
        self.expect(TestBase.CtrlOutput.MoveDownRelais, 1)
        self.expect(TestBase.CtrlOutput.MoveUpRelais, 0)
        self.wait(100)
        self.setShaftSignal(TestBase.ShaftPos.Top, 0)

        self.wait(3000)

        self.setShaftSignal(TestBase.ShaftPos.Bottom, 1)
        self.wait(300)
        self.expect(TestBase.CtrlOutput.MoveDownRelais, 0)
        self.expect(TestBase.CtrlOutput.MoveUpRelais, 0)
        self.expect(TestBase.CtrlOutput.LightBarrier, 0)

        # Terminate the test
        self.m_isRunning = False
        self.sendTestEnd()
