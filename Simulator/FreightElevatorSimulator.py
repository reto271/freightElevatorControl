#!/usr/bin/python3
# encoding: utf-8

import sys
import time
sys.path.append('./Common')

import os
import socket
import myUtils
#import argparse

from junit_xml import TestSuite, TestCase

import SimulatorMsg
from SimulatorMsg import SimulationMsg

from DebugLogger import DebugLogger

# Tests
from Test01_moveTopBottom import Test01_moveTopBottom
from Test02_moveBottomTop import Test02_moveBottomTop
from Test03_moveMiddleBottom import Test03_moveMiddleBottom
from Test04_moveDownStopMoveUp import Test04_moveDownStopMoveUp
from Test05_moveStopImmediatelyReverse import Test05_moveStopImmediatelyReverse
from Test06_moveStopMoveStopMove_noDirectionChange import Test06_moveStopMoveStopMove_noDirectionChange
from Test07_upAndDownKeyAtOnce import Test07_upAndDownKeyAtOnce
from Test08_topBottomSignalAtSameTime import Test08_topBottomSignalAtSameTime
from Test09_atTopMoveUpRequest import Test09_atTopMoveUpRequest
from Test10_moveStopImmediatelyReverseTryAgain import Test10_moveStopImmediatelyReverseTryAgain
from Test11_invalidShaftStateAtStandstill import Test11_invalidShaftStateAtStandstill
from Test12_atBottomMoveDownRequest import Test12_atBottomMoveDownRequest

#---------------------------------------------------------------------------
## Main entry point of the freight elevator controller simulator.
#
def main():
#    createDirectory('ElevatorControl/log')
    myUtils.createDirectory('Test/integrationTestFeedback')

    #parser = parseCmdLine()
    #options = parser.parse_args()

    print('---------------------------------------')
    print('SIMULATOR : Freight Elevator Controller')
    myUtils.printVersionNumber()
    print('')

    m_logger = DebugLogger('S')

    testList = []
    testList.append('Test01_moveTopBottom')
    testList.append('Test02_moveBottomTop')
    testList.append('Test03_moveMiddleBottom')
    testList.append('Test04_moveDownStopMoveUp')
    testList.append('Test05_moveStopImmediatelyReverse')
    testList.append('Test06_moveStopMoveStopMove_noDirectionChange')
    testList.append('Test07_upAndDownKeyAtOnce')
    testList.append('Test08_topBottomSignalAtSameTime')
    testList.append('Test09_atTopMoveUpRequest')
    testList.append('Test10_moveStopImmediatelyReverseTryAgain')
    testList.append('Test11_invalidShaftStateAtStandstill')
    testList.append('Test12_atBottomMoveDownRequest')


    overallTestFeedback = True
    testCases = []
    for testName in testList:
        if (True == overallTestFeedback):
            m_logger.logText('------------------------------------------')
            m_logger.logText('Execute test: ' + testName)
            testFeedback = prepareAndRunSingleTest(testName, m_logger, testCases)
            overallTestFeedback = overallTestFeedback and testFeedback
        else:
            m_logger.logText('------------------------------------------')
            m_logger.logText('NOT Execute test: ' + testName)

    # Collect Test Results
    testSuite = TestSuite('Freight Elevator Integration Tests', testCases)
    with open('Test/integrationTestFeedback/integrationTestResults.xml', 'w') as f:
        TestSuite.to_file(f, [testSuite], prettyprint=True)

    # on the shell 0 means successful
    shellFeedback = (False == overallTestFeedback)
    return shellFeedback


def prepareAndRunSingleTest(testName, logger, testCases):
    feedback = False
    Host = '127.0.0.1'  # Standard loopback interface address (localhost)
    Port = 65000        # Port to listen on (non-privileged ports are > 1023)
    startTime = time.monotonic()
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.bind((Host, Port))
        s.listen()
        time.sleep(1)
        os.system('python3 ./ElevatorControl/FreightElevatorController_main.py -s &')
        tcpSocket, addr = s.accept()
        testObj = getTestByName(testName, tcpSocket, logger)
        if testObj:
            with tcpSocket:
                logger.logText('Connected by: ' + str(addr))

                # Boot procedure
                assert expectBootMsg(tcpSocket, logger)
                sendBootMsg(tcpSocket)
                logger.logText('Simulator booted and connected, correct protocol version')

                # Run the test
                feedback = runSingleTest(testObj)
                logger.logText('Feedback test "' + testName + '" : ' + str(feedback))
        tcpSocket.close()
    endTime = time.monotonic()
    testTime = int(1000*(endTime - startTime)) / 1000
    logger.logText('Test time: ' + str(testTime))
    logger.logText('Overall Test feedback: ' + str(feedback))
    tc = None
    if feedback:
        tc = TestCase(testName, 'FreightElevatorIntegrationTests', testTime, 'Result', '')
    else:
        tc = TestCase(testName, 'FreightElevatorIntegrationTests', testTime, 'Result', testObj.getErrorString())
        tc.add_error_info('FAILED')
    testCases.append(tc)
    return feedback


def runSingleTest(test): #testId, testName, queueSize, tcpSocket, logger):
    test.setup(None)
    test.start()

    # Give the test task time to start, otherwise the while below terminates right away
    time.sleep(0.2)

    while (test.isRunning()):
        data = test.getSocket().recv(1024)
        if data:
            simMsg = SimulatorMsg.deserializeMsg(data)
            SimulatorMsg.printMsg(simMsg, test.getLogger())
            test.putSimulatorMsg(simMsg)
    test.join()
    overallTestFeedback = test.getFeedback()
    if (True == overallTestFeedback):
        test.getLogger().logText('[[SUCCESS]]')
    else:
        test.getLogger().logText('[[FAILED]]')

    return (overallTestFeedback)


def getTestByName(testName, tcpSocket, logger):
    if (testName == 'Test01_moveTopBottom'):
        testObj = Test01_moveTopBottom(1, "Test01_moveTopBottom", 10, tcpSocket, logger)
    elif (testName == 'Test02_moveBottomTop'):
        testObj = Test02_moveBottomTop(2, "Test02_moveBottomTop", 10, tcpSocket, logger)
    elif (testName == 'Test03_moveMiddleBottom'):
        testObj = Test03_moveMiddleBottom(3, "Test03_moveMiddleBottom", 10, tcpSocket, logger)
    elif (testName == 'Test04_moveDownStopMoveUp'):
        testObj = Test04_moveDownStopMoveUp(4, 'Test04_moveDownStopMoveUp', 10, tcpSocket, logger)
    elif (testName == 'Test05_moveStopImmediatelyReverse'):
        testObj = Test05_moveStopImmediatelyReverse(5, 'Test05_moveStopImmediatelyReverse', 10, tcpSocket, logger)
    elif (testName == 'Test06_moveStopMoveStopMove_noDirectionChange'):
        testObj = Test06_moveStopMoveStopMove_noDirectionChange(6, 'Test06_moveStopMoveStopMove_noDirectionChange', 10, tcpSocket, logger)
    elif (testName == 'Test07_upAndDownKeyAtOnce'):
        testObj = Test07_upAndDownKeyAtOnce(7, 'Test07_upAndDownKeyAtOnce', 10, tcpSocket, logger)
    elif (testName == 'Test08_topBottomSignalAtSameTime'):
        testObj = Test08_topBottomSignalAtSameTime(8, 'Test08_topBottomSignalAtSameTime', 10, tcpSocket, logger)
    elif (testName == 'Test09_atTopMoveUpRequest'):
        testObj = Test09_atTopMoveUpRequest(9, 'Test09_atTopMoveUpRequest', 10, tcpSocket, logger)
    elif (testName == 'Test10_moveStopImmediatelyReverseTryAgain'):
        testObj = Test10_moveStopImmediatelyReverseTryAgain(10, 'Test10_moveStopImmediatelyReverseTryAgain', 10, tcpSocket, logger)
    elif (testName == 'Test11_invalidShaftStateAtStandstill'):
        testObj = Test11_invalidShaftStateAtStandstill(10, 'Test11_invalidShaftStateAtStandstill', 10, tcpSocket, logger)
    elif (testName == 'Test12_atBottomMoveDownRequest'):
        testObj = Test12_atBottomMoveDownRequest(11, 'Test12_atBottomMoveDownRequest', 10, tcpSocket, logger)
    else:
        testObj = None
    return testObj


def expectBootMsg(tcpSocket, logger):
    data = tcpSocket.recv(1024)
    if  data:
        simMsg = SimulatorMsg.deserializeMsg(data)
        SimulatorMsg.printMsg(simMsg, logger)
        if ((simMsg.getId() == SimulationMsg.EMsg.Boot) and
            (simMsg.getData() == SimulatorMsg.ProtocolVersion.ProtocolVersion_1)):
            return True
    return False


def sendBootMsg(tcpSocket):
    SimulatorMsg.serializeAndSend(tcpSocket,
                                  SimulationMsg(SimulationMsg.EMsg.Boot,
                                                SimulatorMsg.ProtocolVersion.ProtocolVersion_1))


#- #---------------------------------------------------------------------------
#- ## Defines the command line arguments
#- def parseCmdLine():
#-     ''' Parse the command line. '''
#-     parser = argparse.ArgumentParser(description='Freight Elevator Controller')
#-     parser.add_argument('-s', '--simulation', default=False, action='store_true',
#-                         help='Start with IOHandlerSim (TCP)')
#-     return parser


#---------------------------------------------------------------------------
if __name__ == '__main__':
    exit(main())
