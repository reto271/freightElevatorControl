#!/usr/bin/python3
# encoding: utf-8

import sys
sys.path.append('./Common')

from TestBase import TestBase


## Test 07
#
# Middle position, press up and down key at the same time
class Test07_upAndDownKeyAtOnce(TestBase):

    ## Constructor, requires a reference to the IIOHandler
    def __init__(self, threadId, threadName, msgQueueSize, tcpSocket, debugLogger):
        super(Test07_upAndDownKeyAtOnce, self).__init__(threadId, threadName, msgQueueSize, tcpSocket, debugLogger)
        self.m_logger = debugLogger

    ## Test function
    def run(self):
        self.m_isRunning = True

        #Initialize at top
        # def setIntialStates(self, buttonUp, buttonDown, shaftTop, shaftBottom):
        self.setIntialStates(0, 0, 0, 0)

        self.wait(1000)
        self.expect(TestBase.CtrlOutput.LightBarrier, 0)
        self.expect(TestBase.CtrlOutput.MoveDownRelais, 0)
        self.expect(TestBase.CtrlOutput.MoveUpRelais, 0)

        # The first shall win, the second stop
        self.pressButton(TestBase.Button.Up)
        self.wait(20)
        self.pressButton(TestBase.Button.Down)
        self.wait(300)

        # Wait move properly stopped, relais never switched on
        self.expect(TestBase.CtrlOutput.MoveDownRelais, 0)
        self.expect(TestBase.CtrlOutput.MoveUpRelais, 0)
        self.wait(200)
        for x in range(3):
            self.checkMoveStop()

        # Terminate the test
        self.m_isRunning = False
        self.sendTestEnd()
