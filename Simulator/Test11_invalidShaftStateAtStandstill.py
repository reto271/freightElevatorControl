#!/usr/bin/python3
# encoding: utf-8

import sys
sys.path.append('./Common')

from TestBase import TestBase


## Test 11
#
# Top and bottom signal active at the same time. Expect elevtor controller to stop.
class Test11_invalidShaftStateAtStandstill(TestBase):

    ## Constructor, requires a reference to the IIOHandler
    def __init__(self, threadId, threadName, msgQueueSize, tcpSocket, debugLogger):
        super(Test11_invalidShaftStateAtStandstill, self).__init__(threadId, threadName, msgQueueSize, tcpSocket, debugLogger)
        self.m_logger = debugLogger

    ## Test function
    def run(self):
        self.m_isRunning = True

        #Initialize at middle
        # def setIntialStates(self, buttonUp, buttonDown, shaftTop, shaftBottom):
        self.setIntialStates(0, 0, 0, 0)

        self.wait(1000)
        self.expect(TestBase.CtrlOutput.LightBarrier, 0)
        self.expect(TestBase.CtrlOutput.MoveDownRelais, 0)
        self.expect(TestBase.CtrlOutput.MoveUpRelais, 0)

        # Shaft state gets invalid
        self.setShaftSignal(TestBase.ShaftPos.Top, 1)
        self.setShaftSignal(TestBase.ShaftPos.Bottom, 1)
        self.wait(1000)

        # Move not started
        self.m_logger.logText('----- Start command not accepted, shaft state invalid (still in timeout) -----')
        self.pressButton(TestBase.Button.Down)
        # The car stopps immediately
        for x in range(5):
            self.checkMoveStop()

        # Previous move request, was still timeout before accepting a new move. Wait and try again...
        self.wait(2000)
        self.m_logger.logText('----- Start command not accepted, shaft state invalid (try after timeout) -----')
        self.pressButton(TestBase.Button.Down)
        # The car stopps immediately
        self.wait(100)
        for x in range(5):
            self.checkMoveStop()

        # Terminate the test
        self.m_isRunning = False
        self.sendTestEnd()
