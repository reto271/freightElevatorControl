#!/usr/bin/python3
# encoding: utf-8

import sys
sys.path.append('./Common')

from TestBase import TestBase


## Test 06
#
# Move from top to bottom, stop and continoue in the same direction.
class Test06_moveStopMoveStopMove_noDirectionChange(TestBase):

    ## Constructor, requires a reference to the IIOHandler
    def __init__(self, threadId, threadName, msgQueueSize, tcpSocket, debugLogger):
        super(Test06_moveStopMoveStopMove_noDirectionChange, self).__init__(threadId, threadName, msgQueueSize, tcpSocket, debugLogger)
        self.m_logger = debugLogger

    ## Test function
    def run(self):
        self.m_isRunning = True

        #Initialize at top
        # def setIntialStates(self, buttonUp, buttonDown, shaftTop, shaftBottom):
        self.setIntialStates(0, 0, 1, 0)

        self.wait(1000)
        self.expect(TestBase.CtrlOutput.LightBarrier, 0)
        self.expect(TestBase.CtrlOutput.MoveDownRelais, 0)
        self.expect(TestBase.CtrlOutput.MoveUpRelais, 0)

        self.pressButton(TestBase.Button.Down)
        self.checkMoveStartAndSimStartShaftTransitions(TestBase.Direction.Down)

        self.wait(3000)

        # Stop the car
        self.pressButton(TestBase.Button.Up)
        self.checkMoveStop()

        # No delay start move in same direction immediately
        self.m_logger.logText('----- Restart immediately -----')
        self.pressButton(TestBase.Button.Down)

        # The move does not start, the controller rejects it
        for x in range(10):
            self.expect(TestBase.CtrlOutput.LightBarrier, 0)
            self.expect(TestBase.CtrlOutput.MoveDownRelais, 0)
            self.expect(TestBase.CtrlOutput.MoveUpRelais, 0)
            self.wait(50)


        #Restart move in same direction again, after the relaxation time
        self.wait(2000)
        self.m_logger.logText('----- Start move again -----')
        self.pressButton(TestBase.Button.Down)
        self.checkMoveStartAndSimStartShaftTransitions(TestBase.Direction.Down)

        self.wait(2000)
        self.m_logger.logText('----- Bottom reached -----')
        self.setShaftSignal(TestBase.ShaftPos.Bottom, 1)
        self.checkMoveStop()

        # Terminate the test
        self.m_isRunning = False
        self.sendTestEnd()
