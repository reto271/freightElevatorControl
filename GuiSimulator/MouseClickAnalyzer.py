#!/usr/bin/python3
# encoding: utf-8

# Import libs
import pygame

class MouseClickAnalyzer():
    def __init__(self, xpos, ypos, xwidth, ywidth):
        self.x1 = xpos
        self.y1 = ypos
        self.x2 = xpos + xwidth
        self.y2 = ypos + ywidth

    def draw(self, screen):
        color = (255,0,0)
        # Rect(left, top, width, height) -> Rect
        # pygame.draw.rect(surface, color, pygame.Rect(30, 30, 60, 60))
        r1 = pygame.Rect(self.x1, self.y1, self.x2 - self.x1, self.y2 - self.y1)
        pygame.draw.rect(screen, color, r1)

    def isClick(self, mousePos):
        xpos = mousePos[0]
        ypos = mousePos[1]
        #print('x: ' + str(xpos) + ' / ' + str(ypos))
        if ((self.x1 < xpos) and (xpos < self.x2) and
            (self.y1 < ypos) and (ypos < self.y2)):
            #print('click')
            return True
        return False
