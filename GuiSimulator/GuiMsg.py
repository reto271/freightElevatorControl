#!/usr/bin/python3

from enum import Enum


class GuiMsg():

    class GuiMsgId(Enum):
        MoveDownRelais = 0
        MoveUpRelais = 1
        LightBarrier = 2
        Terminate = 3

    def __init__(self, msgType, msgData):
        self.m_type = msgType
        self.m_data = msgData

    def getType(self):
        return self.m_type

    def getData(self):
        return self.m_data
