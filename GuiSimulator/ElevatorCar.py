#!/usr/bin/python3
# encoding: utf-8

from enum import Enum

class ElevatorCar:
    class Position(Enum):
        Top = 1
        Middle = 2
        Bottom = 3
        Unknown = 4

    def __init__(self, image, xPos, initialHeight, speed):
        self.m_speed = speed
        self.m_actSpeed = 0
        self.image = image
        self.m_pos = image.get_rect().move(0, initialHeight)
        self.m_pos.x = xPos
        self.m_posEnum = ElevatorCar.Position.Unknown
        self.m_posEnumOld = ElevatorCar.Position.Unknown
        self.m_isMoving = False

    def move(self):
        self.m_pos = self.m_pos.move(0, self.m_actSpeed)
        if (self.m_pos.y > 630):
            self.m_actSpeed = 0 #-abs(self.m_actSpeed)
            self.m_posEnum = ElevatorCar.Position.Bottom
            self.m_isMoving = False
        elif (self.m_pos.y < 37):
            self.m_actSpeed = 0 #abs(self.m_actSpeed)
            self.m_posEnum = ElevatorCar.Position.Top
            self.m_isMoving = False
        else:
            self.m_posEnum = ElevatorCar.Position.Middle

    def setMoveDirectionUp(self):
        self.m_actSpeed = -self.m_speed
        self.m_isMoving = True

    def setMoveDirectionDown(self):
        self.m_actSpeed = self.m_speed
        self.m_isMoving = True

    def setMoveStop(self):
        self.m_actSpeed = 0

    def getPosition(self):
        return self.m_posEnum

    def isMoving(self):
        return self.m_isMoving

    def didChange(self):
        posChange = (self.m_posEnum != self.m_posEnumOld)
        self.m_posEnumOld = self.m_posEnum
        return posChange
