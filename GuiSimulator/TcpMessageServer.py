#!/usr/bin/python3
# encoding: utf-8

import sys
import time
sys.path.append('./Common')

import os
#import time
import socket
#import myUtils

from TaskBase import TaskBase
import SimulatorMsg
from SimulatorMsg import SimulationMsg
from GuiMsg import GuiMsg
#from DebugLogger import DebugLogger


class TcpMessageServer(TaskBase):
    def __init__(self, threadId, threadName, debugLogger, autoStart):
        super(TcpMessageServer, self).__init__(threadId, threadName, debugLogger)
        self.m_logger = debugLogger
        self.m_isRunning = False
        self.m_tcpSocket = None
        self.m_msgQueue = None
        self.m_msgQueueMutex = None
        self.m_startElevatorController = autoStart

    def setup(self, msgQueueMainLoop, msgQueueMutex):
        self.m_msgQueue = msgQueueMainLoop
        self.m_msgQueueMutex = msgQueueMutex

    def run(self):
        #Host = '127.0.0.1'  # Standard loopback interface address (localhost)
        #Host = '192.168.213.119'
        Host = ''
        Port = 65000        # Port to listen on (non-privileged ports are > 1023)

        assert(self.m_msgQueue != None)
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.bind((Host, Port))
            s.listen()
            if (True == self.m_startElevatorController):
                time.sleep(1)
                os.system('python3 ./ElevatorControl/FreightElevatorController_main.py -s &')
            tcpSocket, addr = s.accept()

            with tcpSocket:
                self.m_logger.logText('Connected by: ' + str(addr))
                self.m_tcpSocket = tcpSocket

                # Boot procedure
                assert expectBootMsg(tcpSocket, self.m_logger)
                sendBootMsg(tcpSocket)
                self.m_logger.logText('Simulator booted and connected, correct protocol version')

                # Run the simulator
                self.m_isRunning = True
                while (self.m_isRunning):
                    data = tcpSocket.recv(1024)
                    if data:
                        simMsg = SimulatorMsg.deserializeMsg(data)
                        SimulatorMsg.printMsg(simMsg, self.m_logger)
                        self.processTcpMsg(simMsg)
                        #test.putSimulatorMsg(simMsg)
                        #test.join()

            tcpSocket.close()


    def processTcpMsg(self, simMsg):
        if(SimulationMsg.EMsg.LightBarrier == simMsg.getId()):
            print('lb          ----')
            self.__putMsgQueue(GuiMsg(GuiMsg.GuiMsgId.LightBarrier, simMsg.getData()))
        elif(SimulationMsg.EMsg.MoveUpRelais == simMsg.getId()):
            print('up          ----')
            self.__putMsgQueue(GuiMsg(GuiMsg.GuiMsgId.MoveUpRelais, simMsg.getData()))
        elif(SimulationMsg.EMsg.MoveDownRelais == simMsg.getId()):
            print('down          ----')
            self.__putMsgQueue(GuiMsg(GuiMsg.GuiMsgId.MoveDownRelais, simMsg.getData()))
        else:
            assert False

    def sendMoveUpButtonPressed(self):
        self.sendInputMsg(SimulationMsg.EMsg.ButtonUpPressed, 1)
        time.sleep(0.1)
        self.sendInputMsg(SimulationMsg.EMsg.ButtonUpPressed, 0)

    def sendMoveDownButtonPressed(self):
        self.sendInputMsg(SimulationMsg.EMsg.ButtonDownPressed, 1)
        time.sleep(0.1)
        self.sendInputMsg(SimulationMsg.EMsg.ButtonDownPressed, 0)

    def signalTopReached(self):
        self.sendInputMsg(SimulationMsg.EMsg.ShaftSignalTop, 1)

    def signalMiddleReached(self):
        self.sendInputMsg(SimulationMsg.EMsg.ShaftSignalTop, 0)
        time.sleep(0.3)
        self.sendInputMsg(SimulationMsg.EMsg.ShaftSignalBottom, 0)

    def signalBottomReached(self):
        self.sendInputMsg(SimulationMsg.EMsg.ShaftSignalBottom, 1)

    def isStartedUp(self):
        return self.m_isRunning

    def stopTcpServer(self):
        self.m_logger.logText('Terminate TCP Server')
        self.m_isRunning = False
        SimulatorMsg.serializeAndSend(self.m_tcpSocket, SimulationMsg(SimulationMsg.EMsg.TerminateTest, 0))

    def __putMsgQueue(self, msg):
        self.m_msgQueueMutex.acquire()
        self.m_msgQueue.put(msg)
        self.m_msgQueueMutex.release()


    def setIntialStates(self, buttonUp, buttonDown, shaftTop, shaftBottom):
        self.m_logger.logText('Send initial states')
        self.sendInputMsg(SimulationMsg.EMsg.ButtonUpPressed, buttonUp)
        time.sleep(0.3)
        self.sendInputMsg(SimulationMsg.EMsg.ButtonDownPressed, buttonDown)
        time.sleep(0.3)
        self.sendInputMsg(SimulationMsg.EMsg.ShaftSignalTop, shaftTop)
        time.sleep(0.3)
        self.sendInputMsg(SimulationMsg.EMsg.ShaftSignalBottom, shaftBottom)
        time.sleep(0.3)


    def sendInputMsg(self, msgId, value):
        SimulatorMsg.serializeAndSend(self.m_tcpSocket, SimulationMsg(msgId, value))

def expectBootMsg(tcpSocket, logger):
    data = tcpSocket.recv(1024)
    if  data:
        simMsg = SimulatorMsg.deserializeMsg(data)
        SimulatorMsg.printMsg(simMsg, logger)
        if ((simMsg.getId() == SimulationMsg.EMsg.Boot) and
            (simMsg.getData() == SimulatorMsg.ProtocolVersion.ProtocolVersion_1)):
            return True
    return False


def sendBootMsg(tcpSocket):
    SimulatorMsg.serializeAndSend(tcpSocket,
                                  SimulationMsg(SimulationMsg.EMsg.Boot,
                                                SimulatorMsg.ProtocolVersion.ProtocolVersion_1))
