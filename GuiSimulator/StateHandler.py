#!/usr/bin/python3
# encoding: utf-8

# Importieren der Pygame-Bibliothek
#import pygame
#import time
#import sys
#sys.path.append('./Common')
#import queue
#import threading
#
#import myUtils
#from DebugLogger import DebugLogger
#from ElevatorCar import ElevatorCar
#from MouseClickAnalyzer import MouseClickAnalyzer
#from TcpMessageServer import TcpMessageServer
#from GuiMsg import GuiMsg


class StateHandler():
    def __init__(self, logger):
        self.m_logger = logger
        self.m_downMoveRelais = 0
        self.m_upMoveRelais = 0
        self.m_lightBarrier = 0
        self.m_downMoveRelaisOld = 0
        self.m_upMoveRelaisOld = 0
        self.m_lightBarrierOld = 0

    def setDownRelais(self, state):
        self.m_downMoveRelais = state
        if (self.m_downMoveRelais != self.m_downMoveRelaisOld):
            self.m_downMoveRelaisOld = self.m_downMoveRelais
            if (self.m_logger):
                self.m_logger.logText('New state down relais: ' + str(self.m_downMoveRelais))

    def setUpRelais(self, state):
        self.m_upMoveRelais = state
        if (self.m_upMoveRelais != self.m_upMoveRelaisOld):
            self.m_upMoveRelaisOld = self.m_upMoveRelais
            if (self.m_logger):
                self.m_logger.logText('New state up relais: ' + str(self.m_upMoveRelais))

    def setLightBarrier(self, state):
        self.m_lightBarrier = state
        if (self.m_lightBarrier != self.m_lightBarrierOld):
            self.m_lightBarrierOld = self.m_lightBarrier
            if (self.m_logger):
                self.m_logger.logText('New state light barrier: ' + str(self.m_lightBarrier))

    def getDownRelais(self):
        return self.m_downMoveRelais

    def getUpRelais(self):
        return self.m_upMoveRelais

    def getLightBarrier(self):
        return self.m_lightBarrier

    def reevaluateState(self, cabine):
        if (1 == self.m_downMoveRelais):
            if (0 == self.m_upMoveRelais):
                cabine.setMoveDirectionDown()
            else:
                self.m_logger.logText('!!! Both relais on !!!')
                assert(False)
        else:
            if (0 == self.m_upMoveRelais):
                cabine.setMoveStop()
            else:
                cabine.setMoveDirectionUp()
