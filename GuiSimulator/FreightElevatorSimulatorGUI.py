#!/usr/bin/python3
# encoding: utf-8

# Importieren der Pygame-Bibliothek
import pygame
import time
import sys
sys.path.append('./Common')
import queue
import threading
import argparse

import myUtils
from DebugLogger import DebugLogger
from ElevatorCar import ElevatorCar
from MouseClickAnalyzer import MouseClickAnalyzer
from TcpMessageServer import TcpMessageServer
from GuiMsg import GuiMsg
from StateHandler import StateHandler

#---------------------------------------------------------------------------
## Main entry point of the freight elevator controller simulator.
#
def main():

    # Parse the command line for options
    parser = parseCmdLine()
    options = parser.parse_args()

    m_logger = DebugLogger('S')
    tcpSrv = TcpMessageServer(1, 'tcpServer', m_logger, options.auto)

    m_logger = DebugLogger('S')
    msgQueueSize = 10
    messageQueue = queue.Queue(msgQueueSize)
    messageQueueMutex = threading.Lock()
    tcpSrv.setup(messageQueue, messageQueueMutex)

    tcpSrv.start()
    stateHdl = StateHandler(m_logger)

    m_logger.logText('wait for EC...')
    while (not tcpSrv.isStartedUp()):
        time.sleep(1)
        m_logger.logText('wait for EC...')


    #def setIntialStates(self, buttonUp, buttonDown, shaftTop, shaftBottom):
    m_logger.logText('send initial states')
    tcpSrv.setIntialStates(0, 0, 0, 0)
    m_logger.logText('send initial states - done')

    # Initialize pygame
    pygame.init()

    # Define screen area and the images
    screen = pygame.display.set_mode((410, 800))
    cabineImage = pygame.image.load('./GuiSimulator/car.png').convert()
    background = pygame.image.load('./GuiSimulator/background.png').convert()
    screen.blit(background, (0, 0))  # Draw background image
    pygame.display.set_caption('Freight Elevator Controller Simulator       ')

    # Define the car parameters
    speed = 1
    sleepTime = 10
    cabine = ElevatorCar(cabineImage, 42, 240, speed)

    # Define the mouse buttons/area
    mouseAreaDownAtTop = MouseClickAnalyzer(326, 42, 60, 62)
    mouseAreaUpAtTop = MouseClickAnalyzer(254, 42, 60, 62)
    mouseAreaDownAtBottom = MouseClickAnalyzer(326, 698, 60, 62)
    mouseAreaUpAtBottom = MouseClickAnalyzer(254, 698, 60, 62)

    # Draw the button area to define the area properly
    #mouseAreaDownAtTop.draw(screen)
    #mouseAreaUpAtTop.draw(screen)
    #mouseAreaDownAtBottom.draw(screen)
    #mouseAreaUpAtBottom.draw(screen)

    print('-------------------------------------------')
    print('GUI-SIMULATOR : Freight Elevator Controller')
    myUtils.printVersionNumber()
    if True == options.auto:
        print('  Start application automatically')
    else:
        print('  Please start application manually')
    print('')

    initialize = True
    while 1:
        for event in pygame.event.get():
            # Close button at top of window
            if event.type == pygame.QUIT:
                terminateSimulation(tcpSrv)
            # Keyboard inputs
            elif event.type == pygame.KEYDOWN:
                if (event.key == pygame.K_u):
                    tcpSrv.sendMoveUpButtonPressed()
                if (event.key == pygame.K_d):
                    tcpSrv.sendMoveDownButtonPressed()
                if (event.key == pygame.K_q):
                    terminateSimulation(tcpSrv)
            # Handle mouse
            if event.type == pygame.MOUSEBUTTONDOWN:
                pos = pygame.mouse.get_pos()
                if mouseAreaDownAtTop.isClick(pos):
                    tcpSrv.sendMoveDownButtonPressed()
                elif mouseAreaUpAtTop.isClick(pos):
                    tcpSrv.sendMoveUpButtonPressed()
                elif mouseAreaDownAtBottom.isClick(pos):
                    tcpSrv.sendMoveDownButtonPressed()
                elif mouseAreaUpAtBottom.isClick(pos):
                    tcpSrv.sendMoveUpButtonPressed()

        # Got message from Application
        messageQueueMutex.acquire()
        if not messageQueue.empty():
            msg = messageQueue.get()
            messageQueueMutex.release()
            processMessage(msg, m_logger, stateHdl, cabine)
            evaluateAndSendNewShaftPosition(m_logger, stateHdl, cabine, tcpSrv)
        else:
            messageQueueMutex.release()

            # Show state changes in log
#            if (proxy_downMoveRelais != old_downMoveRelais):
#                old_downMoveRelais = proxy_downMoveRelais
#                m_logger.logText('New State down move relais: ' + str(proxy_downMoveRelais))
##            proxy_upMoveRelais  ==         old_upMoveRelais
##            proxy_lightBarrier ==            old_lightBarrier
#            if (proxy_lightBarrier != old_lightBarrier):
#                old_lightBarrier = proxy_lightBarrier
#                m_logger.logText('New State down move relais: ' + str(proxy_lightBarrier))
#
#            m_logger.logText('Current state light barrier relais: ' + str(proxy_lightBarrier))


        if ((True == cabine.isMoving()) or (True == initialize)):
            initialize = False
            screen.blit(background, cabine.m_pos, cabine.m_pos) # clear cabine at old position
            cabine.move()                                       # move it
            screen.blit(cabine.image, cabine.m_pos)             # draw the cabine at the new position
            evaluateAndSendNewShaftPosition(m_logger, stateHdl, cabine, tcpSrv)

            # Update the display and sleep
            pygame.display.update()
            pygame.time.delay(sleepTime)
        else:
            pygame.time.delay(30*sleepTime)

# Process the messages from the applicaton (Sim IO Handler of the elevator controller)
def processMessage(msg, logger, stateHdl, cabine):
    if (GuiMsg.GuiMsgId.MoveDownRelais == msg.getType()):
        stateHdl.setDownRelais(msg.getData())
    if (GuiMsg.GuiMsgId.MoveUpRelais == msg.getType()):
        stateHdl.setUpRelais(msg.getData())
    if (GuiMsg.GuiMsgId.LightBarrier == msg.getType()):
        stateHdl.setLightBarrier(msg.getData())
    stateHdl.reevaluateState(cabine)

def evaluateAndSendNewShaftPosition(logger, stateHdl, cabine, tcpSrv):
    if (1 == stateHdl.getLightBarrier()):
        # Update Shaft State
        if (cabine.didChange()):
            if (cabine.getPosition() == ElevatorCar.Position.Top):
                tcpSrv.signalTopReached()
            elif (cabine.getPosition() == ElevatorCar.Position.Middle):
                tcpSrv.signalMiddleReached()
            elif (cabine.getPosition() == ElevatorCar.Position.Bottom):
                tcpSrv.signalBottomReached()
    else:
        # Update shaft to default states of sensors
        pass

def terminateSimulation(tcpSrv):
    print('done')
    tcpSrv.stopTcpServer()
    tcpSrv.join()
    pygame.quit()
    sys.exit()


#---------------------------------------------------------------------------
## Defines the command line arguments
def parseCmdLine():
    ''' Parse the command line. '''
    parser = argparse.ArgumentParser(description='Freight Elevator Controller')
    parser.add_argument('-a', '--auto', default=False,
                        action='store_true',
                        help='Automatically starts the Elevator Controller')
    return parser


#---------------------------------------------------------------------------
if __name__ == '__main__':
    exit(main())
