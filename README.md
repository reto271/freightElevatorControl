# Freight Elevator Control
## Abstract
The freight elevator controller is small elevator controller for a freight elevator. The elevator has only two floors. The controller is implemented in python and runs on Raspberry Pi.

## System Overview
System Overview including the simulation infrastructure. The head-less simulator is used for CI (continuous integration) and SW development. The GUI simulator is used for manual tests and interaction with the stakeholder.

![System Overview](Documentation/deloyment.svg)
