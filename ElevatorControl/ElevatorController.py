#!/usr/bin/python3
# encoding: utf-8

from TaskMsgQueue import TaskMsgQueue
import MessageDefinition
from MessageDefinition import TelegramMsg
import ShaftImage
from MoveController import MoveController


## Elevator Controller
#
# Controls the positon and direction of the elevator. Is responsible to handle all requests from
# the users, e.g. key pressed, telegram inputs, ...
class ElevatorController(TaskMsgQueue):


    ## Constructor, requires a reference to the IIHandler
    def __init__(self, threadId, threadName, msgQueueSize, moveCtrl, shaftImage, ioHdl, debugLogger):
        super(ElevatorController, self).__init__(threadId, threadName, msgQueueSize, debugLogger)
        self.m_moveCtrl = moveCtrl
        self.m_shaftImage = shaftImage
        self.m_ioHdl = ioHdl
        self.m_telegramHdl = None
        self.m_logger = debugLogger

    def setup(self, telegramHdl):
        self.m_telegramHdl = telegramHdl

    def processMsg(self, msg):
        assert self.m_telegramHdl

        # Process messages from ShaftImage
        if (MessageDefinition.EleMsg.NewPosition == msg):
            carPos = self.m_shaftImage.getPosition()
            telegramMsg = TelegramMsg(TelegramMsg.Msg2.PositionResponse, carPos)
            self.m_telegramHdl.putMsgToQueue(telegramMsg)
            if (ShaftImage.CarPosition.Bottom == carPos):
                self.m_moveCtrl.request(MoveController.Triggers.ShaftPosBottomReached)
            elif (ShaftImage.CarPosition.Top == carPos):
                self.m_moveCtrl.request(MoveController.Triggers.ShaftPosTopReached)
            elif (ShaftImage.CarPosition.Middle == carPos):
                # No action
                pass
            elif (ShaftImage.CarPosition.Invalid == carPos):
                self.m_moveCtrl.request(MoveController.Triggers.EmergencyStopRequest)
            else:
                self.m_moveCtrl.request(MoveController.Triggers.EmergencyStopRequest)



        # Buttons from the HW pannels
        elif (MessageDefinition.EleMsg.DownButtonPressed == msg):
            self.m_moveCtrl.request(MoveController.Triggers.MoveDownButton)
        elif (MessageDefinition.EleMsg.UpButtonPressed == msg):
            self.m_moveCtrl.request(MoveController.Triggers.MoveUpButton)
        # Requests from Telegram
        elif (MessageDefinition.EleMsg.DownRequest == msg):
            self.m_moveCtrl.request(MoveController.Triggers.MoveDownButton)
        elif (MessageDefinition.EleMsg.UpRequest  == msg):
            self.m_moveCtrl.request(MoveController.Triggers.MoveUpButton)
        elif (MessageDefinition.EleMsg.StopRequest == msg):
            self.m_moveCtrl.request(MoveController.Triggers.StopRequest)
        elif (MessageDefinition.EleMsg.GetPosRequest == msg):
            carPos = self.m_shaftImage.getPosition()
            telegramMsg = TelegramMsg(TelegramMsg.Msg2.PositionResponse, carPos)
            self.m_telegramHdl.putMsgToQueue(telegramMsg)
        elif (MessageDefinition.EleMsg.Terminate == msg):
            pass
        else:
            assert False

    def terminateMsg(self):
        self.putMsgToQueue(MessageDefinition.EleMsg.Terminate)
