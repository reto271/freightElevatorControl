#!/usr/bin/python3
# encoding: utf-8

from enum import Enum
from TaskBase import TaskBase
from abc import ABC, abstractmethod

class IIOHandler(ABC, TaskBase):

    class Input(Enum):
        TopSwitch = 0
        BottomSwitch = 1
        UpButton = 2
        DownButton = 3

    class Output(Enum):
        EnableLightBarrier = 0
        MoveDownRelais = 1
        MoveUpRelais = 2

    def __init__(self, threadId, threadName, debugLogger):
        super(IIOHandler, self).__init__(threadId, threadName, debugLogger)
        self.m_logger = debugLogger
        self.m_topSwitch = None
        self.m_bottomSwitch = None
        self.m_upButton = None
        self.m_downButton = None
        self.m_shaftImage = None


    def setup(self, shaftImage, eleCtrl):
        self.m_shaftImage = shaftImage
        self.m_eleCtrl = eleCtrl


    def getInput(self, inputEnum):
        if (IIOHandler.Input.TopSwitch == inputEnum):
            return self.m_topSwitch
        elif (IIOHandler.Input.BottomSwitch == inputEnum):
            return self.m_bottomSwitch
        elif (IIOHandler.Input.UpButton == inputEnum):
            return self.m_upButton
        elif (IIOHandler.Input.DownButton == inputEnum):
            return self.m_downButton


    def updateMessage(self, eleMsg):
        self.m_eleCtrl.putMsgToQueue(eleMsg)


    @abstractmethod
    def setOutput(self, outputEnum, value):
        pass
