#!/usr/bin/python3

from enum import Enum


## Message received and processed by the ElevatorController
class EleMsg(Enum):

    # Position changed (from ShaftImage)
    NewPosition = 0

    # Buttons from the HW pannels
    DownButtonPressed = 1
    UpButtonPressed = 2

    # Requests from telgram
    DownRequest = 3
    UpRequest = 4
    StopRequest = 5
    GetPosRequest = 6

    # Administration Message
    Terminate = 7


class TelegramMsg():

    class Msg2(Enum):
        PositionResponse = 0
        BootMsg = 1
        Terminate = 2

    def __init__(self, msgType, msgData):
        self.m_type = msgType
        self.m_data = msgData

    def getType(self):
        return self.m_type

    def getData(self):
        return self.m_data
