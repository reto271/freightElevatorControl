#!/usr/bin/python3
# encoding: utf-8

import time

import SimulatorMsg
from SimulatorMsg import SimulationMsg

import MessageDefinition
from IIOHandler import IIOHandler
import socket

class IOHandlerSim(IIOHandler):
    def __init__(self, threadId, threadName, debugLogger, ipAddress):
        super(IOHandlerSim, self).__init__(threadId, threadName, debugLogger)
        self.m_ipAddress = ipAddress

    def run(self):
        assert self.m_shaftImage
        assert self.m_eleCtrl

        # Startup
        Port = 65000        # The port used by the server
        self.m_logger.logText('Connect to ' + str(self.m_ipAddress) + ':' + str(Port))
        self.__connectToServer(str(self.m_ipAddress), Port)

        # Send startup message
        SimulatorMsg.serializeAndSend(self.mySocket,
                         SimulationMsg(SimulationMsg.EMsg.Boot,
                                       SimulatorMsg.ProtocolVersion.ProtocolVersion_1))

        # Expect the boot msg
        assert self.expectBootMsg()
        self.m_logger.logText('Simulator has correct version')

        self.exitFlag = False
        while not self.exitFlag:
            # Poll IOs - wait for messages from simulation
            data = self.mySocket.recv(1024)
            if data:
                simMsg = SimulatorMsg.deserializeMsg(data)
                SimulatorMsg.printMsg(simMsg, self.m_logger)
                self.exitFlag = self.__processInput(simMsg)
        self.m_logger.logText('Terminate "IOHandlerSim"')


    def setOutput(self, outputEnum, value):
        if (IIOHandler.Output.EnableLightBarrier == outputEnum):
            SimulatorMsg.serializeAndSend(self.mySocket,
                                          SimulationMsg(SimulationMsg.EMsg.LightBarrier, value))

        elif (IIOHandler.Output.MoveDownRelais == outputEnum):
            SimulatorMsg.serializeAndSend(self.mySocket,
                                          SimulationMsg(SimulationMsg.EMsg.MoveDownRelais, value))

        elif (IIOHandler.Output.MoveUpRelais == outputEnum):
            SimulatorMsg.serializeAndSend(self.mySocket,
                                          SimulationMsg(SimulationMsg.EMsg.MoveUpRelais, value))
        time.sleep(0.1)


    def __connectToServer(self, Host, Port):
        self.mySocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.mySocket.connect((Host, Port))


    def expectBootMsg(self):
        data = self.mySocket.recv(1024)
        if  data:
            simMsg = SimulatorMsg.deserializeMsg(data)
            SimulatorMsg.printMsg(simMsg, self.m_logger)
            if ((simMsg.getId() == SimulationMsg.EMsg.Boot) and
                (simMsg.getData() == SimulatorMsg.ProtocolVersion.ProtocolVersion_1)):
                return True
        return False


    def __processInput(self, simMsg):
        if (SimulationMsg == type(simMsg)):
            if(SimulationMsg.EMsg.ButtonUpPressed == simMsg.getId()):
                self.m_upButton = simMsg.getData()
                if (True == self.m_upButton):
                    self.updateMessage(MessageDefinition.EleMsg.UpButtonPressed)
            elif(SimulationMsg.EMsg.ButtonDownPressed == simMsg.getId()):
                self.m_downButton = simMsg.getData()
                if (True == self.m_downButton):
                    self.updateMessage(MessageDefinition.EleMsg.DownButtonPressed)
            elif(SimulationMsg.EMsg.ShaftSignalTop == simMsg.getId()):
                self.m_topSwitch = simMsg.getData()
            elif(SimulationMsg.EMsg.ShaftSignalBottom == simMsg.getId()):
                self.m_bottomSwitch = simMsg.getData()
            elif(SimulationMsg.EMsg.TerminateTest == simMsg.getId()):
                return True
            self.m_shaftImage.reevaluatePosition()
            return False
