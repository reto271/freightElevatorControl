#!/usr/bin/python3
# encoding: utf-8

try:
    from gpiozero import Button
    from gpiozero import LED
    from time import sleep
except ImportError:
    print('\nwarning gpiozero could not be imported\n\n')

from IIOHandler import IIOHandler
import MessageDefinition

class IOHandler(IIOHandler):
    def __init__(self, threadId, threadName, debugLogger):
        super(IOHandler, self).__init__(threadId, threadName, debugLogger)
        self.m_gpioTopSwitch = Button(1)
        self.m_gpioBottomSwitch = Button(2)
        self.m_gpioUpButton = Button(3)
        self.m_gpioDownButton = Button(4)
        self.m_gpioLightBarrier = LED(5)
        self.m_gpioRelaisMoveDown = LED(6)
        self.m_gpioRelaisMoveUp = LED(7)

#    def registexprOutput(self):
#        # todo
#        return
#
#    def registerInput(self):
#        # todo
#        return
#
#    def registerCallback(self):
#        # todo
#        return

    def run(self):
        assert self.m_shaftImage
        assert self.m_eleCtrl

        self.exitFlag = False
        while not self.exitFlag:
            sleep(1)
            # Poll IOs
            if (self.m_topSwitch != self.m_gpioTopSwitch.is_pressed):
                self.m_topSwitch = self.m_gpioTopSwitch.is_pressed
                self.m_shaftImage.reevaluatePosition()

            elif(self.m_bottomSwitch != self.m_gpioBottomSwitch.is_pressed):
                self.m_bottomSwitch = self.m_gpioBottomSwitch.is_pressed
                self.m_shaftImage.reevaluatePosition()

            elif(self.m_upButton != self.m_gpioUpButton.is_pressed):
                self.m_upButton = self.m_gpioUpButton.is_pressed
                self.updateMessage(MessageDefinition.EleMsg.UpButtonPressed)

            elif(self.m_downButton != self.m_gpioDownButton.is_pressed):
                self.m_downButton = self.m_gpioDownButton.is_pressed
                self.updateMessage(MessageDefinition.EleMsg.DownButtonPressed)


    def setOutput(self, outputEnum, value):
        if (IIOHandler.Output.EnableLightBarrier == outputEnum):
            if (True == value):
                self.m_gpioLightBarrier.on()
            else:
                self.m_gpioLightBarrier.off()

        elif (IIOHandler.Output.MoveDownRelais == outputEnum):
            if (True == value):
                self.m_gpioRelaisMoveDown.on()
            else:
                self.m_gpioRelaisMoveDown.off()

        elif (IIOHandler.Output.MoveUpRelais == outputEnum):
            if (True == value):
                self.m_gpioRelaisMoveUp.on()
            else:
                self.m_gpioRelaisMoveUp.off()
