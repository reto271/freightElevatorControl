#!/usr/bin/python3
# encoding: utf-8

import sys
sys.path.append('./Common')

import time
import argparse
from ipaddress import ip_address

import myUtils
from DebugLogger import DebugLogger
from IOHandler import IOHandler
from IOHandlerSim import IOHandlerSim
from MoveController import MoveController
from ShaftImage import ShaftImage
from ElevatorController import ElevatorController
from TelegramHandler import TelegramHandler
from MessageDefinition import TelegramMsg   # to be removed

#---------------------------------------------------------------------------
## Main entry point fo the freight elevator controller.
#
# Start the application in real mode or in simulation mode. To start in
# simulation use the option -s or --simulation. Starting without a parameter
# the real application is started.
def main():

    myUtils.createDirectory('ElevatorControl/log')
    myUtils.createDirectory('ElevatorControl/cnfg')

    parser = parseCmdLine()
    options = parser.parse_args()

    print('---------------------------')
    print('Freight Elevator Controller')
    myUtils.printVersionNumber()
    m_isSimulation = None
    if True == options.simulation:
        print('  Mode: Simulation')
        m_isSimulation = True
    else:
        print('  Mode: Controller')
        m_isSimulation = False
    print('---------------------------')
    print('')

    m_logger = DebugLogger('E')
    m_ioHandler = None
    if True == options.simulation:
        if (options.ipaddress):
            ipAddress = options.ipaddress
        else:
            ipAddress = '127.0.0.1'  # The server's hostname or IP address
        m_ioHandler = IOHandlerSim(3, 'IOHdlSim', m_logger, ipAddress)
    else:
        m_ioHandler = IOHandler(3, 'IOHdl', m_logger)

    m_shaftImage = ShaftImage(m_ioHandler, m_logger)
    m_moveCtrl = MoveController(m_ioHandler, m_isSimulation, m_shaftImage, m_logger)
    m_eleCtrl = ElevatorController(2, 'ElevatorController', 10, m_moveCtrl, m_shaftImage, m_ioHandler, m_logger)
    m_telegramHdl = TelegramHandler(1, 'TelegramHandler', 10, m_eleCtrl, m_logger, m_isSimulation)


#    # Create new threads
#    for tName in threadList:
#        thread = ThreadBase(threadID, tName, workQueue)
#        thread.start()
#        threads.append(thread)
#        threadID += 1

    # Setup phase
    m_shaftImage.setup(m_eleCtrl)
    m_eleCtrl.setup(m_telegramHdl)
    m_telegramHdl.setup('./ElevatorControl/cnfg/botId.txt',
                        './ElevatorControl/cnfg/registeredIds.txt',
                        './ElevatorControl/cnfg/notificationIds.txt',
                        './ElevatorControl/cnfg/adminId.txt')
    m_ioHandler.setup(m_shaftImage, m_eleCtrl)

    # Start all treads
    m_ioHandler.start()
    time.sleep(0.5)
    m_eleCtrl.start()
    time.sleep(0.5)
    m_telegramHdl.start()
    time.sleep(0.5)

    m_logger.logText('Successfully booted')
    m_telegramHdl.putMsgToQueue(TelegramMsg(
        TelegramMsg.Msg2.BootMsg,
        'Freight Elevator Controller\n\n' +
        '  Version: ' + myUtils.getVersionNumber() + '\n' +
        '  RunTime:\n' +
        '  Moves Today:\n' +
        '  Moves since Boot:\n' +
        '  Moves total:\n'))

    exitFlag = False
    while not exitFlag:
        if True == options.simulation:
            time.sleep(1)
            if (False == m_ioHandler.is_alive()):
                m_logger.logText('terminated')
                m_eleCtrl.terminate()
                m_telegramHdl.terminate()
                m_eleCtrl.join()
                m_telegramHdl.join()
                exitFlag = True
        else:
            time.sleep(60)
    return 0


#---------------------------------------------------------------------------
## Defines the command line arguments
def parseCmdLine():
    ''' Parse the command line. '''
    parser = argparse.ArgumentParser(description='Freight Elevator Controller')
    parser.add_argument('-s', '--simulation', default=False, action='store_true',
                        help='Start with IOHandlerSim (TCP)')
    parser.add_argument('-i', '--ipaddress', action='store', type=ip_address,
                        help='IP address of the simulator. Default value = 127.0.0.1',
                        required=False)
    return parser


#---------------------------------------------------------------------------
if __name__ == '__main__':
    exit(main())
