#!/usr/bin/env python3
# encoding: utf-8

import unittest

class Test_dummy(unittest.TestCase):
    def __initializeTest(self):
        pass

    def test_true(self):
        self.__initializeTest()
        self.assertEqual(True, True)

    def test_false(self):
        self.__initializeTest()
        #self.assertEqual(True, False)

if __name__ == '__main__':
    import xmlrunner
    unittest.main(testRunner=xmlrunner.XMLTestRunner(output='test-reports'))
    #unittest.main()  # Calling from the command line invokes all tests
