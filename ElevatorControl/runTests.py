#!/usr/bin/python3
# encoding: utf-8

import os
import unittest
import xmlrunner
import shutil
import sys

if __name__ == '__main__':
    root_dir = os.path.dirname(__file__)
    print('root_dir ' + str(root_dir))
    try:
        shutil.rmtree(root_dir + '/test-reports')
    except:
        pass
    test_loader = unittest.TestLoader()
    package_tests = test_loader.discover(root_dir, pattern='Test_*.py')
    print('package_tests: ' + str(package_tests))

    testRunner = xmlrunner.XMLTestRunner(output=root_dir + '/test-reports')
    feedback = testRunner.run(package_tests)
    posError = str(feedback).find('errors=0')
    posFailure = str(feedback).find('failures=0')
    if (-1 == posError) or (-1 ==posFailure):
        sys.exit(1)
    else:
        sys.exit(0)
