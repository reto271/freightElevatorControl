#!/usr/bin/env python3
# encoding: utf-8

import sys
sys.path.append('./Common')

import time
from enum import Enum
from IIOHandler import IIOHandler
import CommonEnums

## Move Controller
#
# Ensures that there is enough time between changing direction.
class MoveController:
    class Triggers(Enum):
        MoveDownButton = 0
        MoveUpButton = 1
        StopRequest = 2
        ShaftPosBottomReached = 3
        ShaftPosTopReached = 4
        EmergencyStopRequest = 5

    class MoveState(Enum):
        Idle = 0
        MovingUp = 1
        MovingDown = 2
        Timeout = 3

    ## Constructor, requires a reference to the IIOHandler
    def __init__(self, ioHdl, isSimulation, shaftImage, debugLogger):
        self.m_ioHdl = ioHdl
        self.m_isSimulation = isSimulation
        self.m_shaftImage = shaftImage
        self.m_logger = debugLogger
        self.m_moveState = MoveController.MoveState.Idle
        self.m_time = time.monotonic()

    ## Implement message based state machine
    def request(self, trigger):
        self.__checkIfIdleTimeExpired()
        if(MoveController.Triggers.MoveDownButton == trigger):
            self.__reqMoveDownButton()
        elif(MoveController.Triggers.MoveUpButton == trigger):
            self.__reqMoveUpButton()
        elif(MoveController.Triggers.StopRequest == trigger):
            self.__reqStopRequest()
        elif(MoveController.Triggers.ShaftPosBottomReached == trigger):
            self.__reqShaftPosBottomReached()
        elif(MoveController.Triggers.ShaftPosTopReached == trigger):
            self.__reqShaftPosTopReached()
        elif(MoveController.Triggers.EmergencyStopRequest == trigger):
            self.__reqEmergencyStopRequest()
        else:
            self.m_logger.logText('MC: Unsupported request')
            assert(False)

    ## -------------------------------------------------------------------
    ## Message based actions
    def __reqMoveDownButton(self):
        if(MoveController.MoveState.Idle == self.m_moveState):
            self.__prepareMoveAndStart(MoveController.Triggers.MoveDownButton)
        elif(MoveController.MoveState.MovingUp == self.m_moveState):
            self.__moveStop()
        elif(MoveController.MoveState.MovingDown == self.m_moveState):
            # Is already moving down, no action
            pass
        elif(MoveController.MoveState.Timeout == self.m_moveState):
            # Ignore request
            pass
        else:
            pass

    def __reqMoveUpButton(self):
        if(MoveController.MoveState.Idle == self.m_moveState):
            self.__prepareMoveAndStart(MoveController.Triggers.MoveUpButton)
        elif(MoveController.MoveState.MovingUp == self.m_moveState):
            # Is already moving up, no action
            pass
        elif(MoveController.MoveState.MovingDown == self.m_moveState):
            self.__moveStop()
        elif(MoveController.MoveState.Timeout == self.m_moveState):
            # Ignore request
            pass
        else:
            pass

    def __reqStopRequest(self):
        if(MoveController.MoveState.Idle == self.m_moveState):
            # Nothing to do
            pass
        elif(MoveController.MoveState.MovingUp == self.m_moveState):
            self.__moveStop()
        elif(MoveController.MoveState.MovingDown == self.m_moveState):
            self.__moveStop()
        elif(MoveController.MoveState.Timeout == self.m_moveState):
            # Nothing to do
            pass
        else:
            pass

    def __reqShaftPosBottomReached(self):
        if(MoveController.MoveState.Idle == self.m_moveState):
            # May occur if the light barrier is switched on
            pass
        elif(MoveController.MoveState.MovingUp == self.m_moveState):
            # Shall not occur at all
            assert(False)
        elif(MoveController.MoveState.MovingDown == self.m_moveState):
            self.__moveStop()
        elif(MoveController.MoveState.Timeout == self.m_moveState):
            # May occur if the light barrier is switched off
            pass
        else:
            pass

    def __reqShaftPosTopReached(self):
        if(MoveController.MoveState.Idle == self.m_moveState):
            # May occur if the light barrier is switched on
            pass
        elif(MoveController.MoveState.MovingUp == self.m_moveState):
            self.__moveStop()
        elif(MoveController.MoveState.MovingDown == self.m_moveState):
            # Shall not occur at all
            assert(False)
        elif(MoveController.MoveState.Timeout == self.m_moveState):
            # May occur if the light barrier is switched off
            pass
        else:
            pass

    def __reqEmergencyStopRequest(self):
        self.m_logger.logText('EMERGENCY STOP')
        self.__moveStop()

#-------------------------------------------------------------------
    def __prepareMoveAndStart(self, direction):
        assert(MoveController.MoveState.Idle == self.m_moveState)
        self.m_logger.logText('__prepareMoveAndStart')
        # Switch on the light barrier as fast as possible
        self.m_ioHdl.setOutput(IIOHandler.Output.EnableLightBarrier, 1)
        # Sleep to get the shaft signals updated ...
        time.sleep(0.1)

        # Move at position allowed?
        msg = None
        if (MoveController.Triggers.MoveDownButton == direction):
            msg = CommonEnums.Command.Down
        elif (MoveController.Triggers.MoveUpButton == direction):
            msg = CommonEnums.Command.Up
        else:
            assert(False)
        if(False == self.m_shaftImage.isAllowed(msg)):
            # To switch off the light barrier
            self.m_logger.logText('Shaft position does not allow the move')
            self.__moveStop()
            return

        # Finally start the move
        if (MoveController.Triggers.MoveDownButton == direction):
            self.m_ioHdl.setOutput(IIOHandler.Output.MoveDownRelais, 1)
            self.m_moveState = MoveController.MoveState.MovingDown
        else:
            self.m_ioHdl.setOutput(IIOHandler.Output.MoveUpRelais, 1)
            self.m_moveState = MoveController.MoveState.MovingUp

    def __moveStop(self):
        # Timeouts between output changes only necessary for simulation (otherwise TCP frames
        #  get collected and the transmission does not work
        # -> Find a better solution. It is only a quick and dirty fix.
        self.m_logger.logText('Process move stop')
        self.m_ioHdl.setOutput(IIOHandler.Output.MoveDownRelais, 0)
        self.__communicationTimeout(0.025)
        self.m_ioHdl.setOutput(IIOHandler.Output.MoveUpRelais, 0)
        self.__communicationTimeout(0.025)
        self.m_ioHdl.setOutput(IIOHandler.Output.EnableLightBarrier, 0)
        self.__communicationTimeout(0.025)
        self.__startIdleTime()

    def __startIdleTime(self):
        self.m_time = time.monotonic()
        self.m_logger.logText('start idle timer: ' + str(self.m_time))
        self.m_moveState = MoveController.MoveState.Timeout

    def __checkIfIdleTimeExpired(self):
        if (self.m_moveState == MoveController.MoveState.Timeout):
            timeNow = time.monotonic()
            self.m_logger.logText('start idle timer: ' + str(self.m_time))
            self.m_logger.logText('current time: ' + str(timeNow))
            self.m_logger.logText('delta time: ' + str(timeNow - self.m_time))
            if ((timeNow - self.m_time) > 2):
                self.m_moveState = MoveController.MoveState.Idle
                self.m_logger.logText('MC: Move State Idle')
            else:
                self.m_logger.logText('MC: Move State still Timeout!')

    def __communicationTimeout(self, timeout):
        if (True == self.m_isSimulation):
            time.sleep(timeout)
