#!/usr/bin/python3

import CommonEnums
import MessageDefinition
from IIOHandler import IIOHandler
from enum import Enum

class CarPosition(Enum):
    Unknown = 0
    Top = 1
    Bottom = 2
    Middle = 3
    Invalid = 4

## The ShaftImage
#
# Controls the positon of the elevator. It determines the state of the car and
# provides the information whether a move in a requested direction is possible or
# not.
class ShaftImage():
    ## Constructor, requires a reference to the ElevatorController to signal position changes and
    # a reference to the io handler to check the io states.
    def __init__(self, ioHdl, debugLogger):
        self.m_eleCtrl = None
        self.m_ioHdl = ioHdl
        self.m_logger = debugLogger
        self.m_pos = CarPosition.Unknown

    def setup(self, eleCtrl):
        self.m_eleCtrl = eleCtrl

    def reevaluatePosition(self):
        assert self.m_eleCtrl

        # Get position switches
        bottomSwitch = self.m_ioHdl.getInput(IIOHandler.Input.BottomSwitch)
        topSwitch = self.m_ioHdl.getInput(IIOHandler.Input.TopSwitch)

        # Evaluate new Position
        newPos = CarPosition.Unknown
        if (True == bottomSwitch):
            if (True == topSwitch):
                newPos = CarPosition.Invalid
            else:
                newPos = CarPosition.Bottom
        else:
            if (True == topSwitch):
                newPos = CarPosition.Top
            else:
                newPos = CarPosition.Middle


        if (newPos != self.m_pos):
            self.__printPosition(newPos)
            self.m_pos = newPos
            self.m_eleCtrl.processMsg(MessageDefinition.EleMsg.NewPosition)


    def getPosition(self):
        return self.m_pos

    def isAllowed(self, command):
        if (CommonEnums.Command.Down == command):
            if ((CarPosition.Top == self.m_pos) or (CarPosition.Middle == self.m_pos)):
                return True
        if (CommonEnums.Command.Up == command):
            if ((CarPosition.Bottom == self.m_pos) or (CarPosition.Middle == self.m_pos)):
                return True
        return False

    def __printPosition(self, newPos):
        self.m_logger.logText('ShaftImage: oldPos: ' +
                              self.strPos(self.m_pos) +
                              ' newPos: ' +
                              self.strPos(newPos))

    def strPos(self, pos):
        if (pos == CarPosition.Unknown):
            return 'Unknown'
        elif (pos == CarPosition.Top):
            return 'Top'
        elif (pos == CarPosition.Bottom):
            return 'Bottom'
        elif (pos == CarPosition.Middle):
            return 'Middle'
        elif (pos == CarPosition.Invalid):
            return 'INVALID'
        else:
            assert(False)
