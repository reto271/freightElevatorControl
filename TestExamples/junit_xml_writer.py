#!/usr/bin/env python3
# encoding: utf-8

#---- Example output from google test
# <?xml version="1.0" encoding="UTF-8"?>
# <testsuites tests="3" failures="1" disabled="0" errors="0" time="0" timestamp="2021-03-16T22:41:56.993" name="AllTests">
#   <testsuite name="Test_BitBuffer" tests="3" failures="1" disabled="0" skipped="0" errors="0" time="0" timestamp="2021-03-16T22:41:56.993">
#     <testcase name="Test_01" status="run" result="completed" time="0" timestamp="2021-03-16T22:41:56.993" classname="Test_BitBuffer" />
#     <testcase name="Test_02" status="run" result="completed" time="0" timestamp="2021-03-16T22:41:56.993" classname="Test_BitBuffer" />
#     <testcase name="Test_03" status="run" result="completed" time="0" timestamp="2021-03-16T22:41:56.993" classname="Test_BitBuffer">
#       <failure message="/home/reto/git/freightElevatorControl/Test/src/Test_BitBuffer.cpp:106&#x0A;Expected equality of these values:&#x0A;  1&#x0A;  0" type=""><![CDATA[/home/reto/git/freightElevatorControl/Test/src/Test_BitBuffer.cpp:106 Expected equality of these values: 1  0]]></failure>
#     </testcase>
#   </testsuite>
# </testsuites>


from junit_xml import TestSuite, TestCase

tc1 = TestCase('Test1', 'some_class_name', 1.12, 'I am stdout!', 'I am stderr!')
tc2 = TestCase('Test2', 'some_class_name', 2.00, 'I am stdout!', 'I am stderr!')
tc3 = TestCase('Test3', 'some_class_name', 10.00, 'I am stdout!', 'I am stderr!')
test_cases = [tc1, tc2, tc3]
ts = TestSuite("my test suite", test_cases)
# pretty printing is on by default but can be disabled using prettyprint=False
print(TestSuite.to_xml_string([ts]))


with open('output.xml', 'w') as f:
    TestSuite.to_file(f, [ts], prettyprint=True)

#---- Output from this script
# <?xml version="1.0" ?>
# <testsuites disabled="0" errors="0" failures="0" tests="1" time="123.345">
# 	<testsuite disabled="0" errors="0" failures="0" name="my test suite" skipped="0" tests="1" time="123.345">
# 		<testcase name="Test1" time="123.345000" classname="some_class_name">
# 			<system-out>I am stdout!</system-out>
# 			<system-err>I am stderr!</system-err>
# 		</testcase>
# 	</testsuite>
# </testsuites>
#
