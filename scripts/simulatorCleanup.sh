#!/bin/bash


function cleanupProcess()
{
    ProcessName=$1
    ProcessSearchString=$2

    echo "---------------------------------------------------------------"
    echo "${ProcessName}"

    searchString=$(ps -U reto -o pid,cmd | grep ${ProcessSearchString} | grep "python3")
    if [ ! -z "${searchString}" ] ; then
        echo "${searchString}"
    fi
    pid=$(echo "${searchString}" | awk '{ print $1 }')

    if [ ! -z ${pid} ] ; then
        echo "pid: ${pid}"
        kill -9 ${pid} &
        sleep 0.5
        searchString2=$(ps -U reto -o pid,cmd | grep ${ProcessSearchString} | grep "python3")
        if [ -z ${searchString2} ] ; then
            echo "  successfuly killed"
        else
            echo "  fail, app still running"
        fi
    else
        echo "not running anymore"
    fi
}

cleanupProcess "Freight Elevator Control Application" "FreightElevatorController_main.py"
cleanupProcess "Freight Elevator GUI-Simulator" "FreightElevatorSimulatorGUI\.py"


# echo ""
# echo "---------------------------------------------------------------"
# echo "Listen Ports..."
# sudo lsof -i -P -n | grep LISTEN

# echo ""
echo "---------------------------------------------------------------"
echo "Is lisenting socket properly closed (CLOSE_WAIT) state?"
lsof -i :65000


#sudo fuser -k 65000/tcp\n

# echo ""
# echo "---------------------------------------------------------------"
echo "Try to kill it ..."
sudo ss --tcp --kill dport = 65000


# echo ""
# echo "---------------------------------------------------------------"
echo "Socket really gone ?"
portSillOpen=$(lsof -i :65000 | wc -l)
if [ 0 -eq ${portSillOpen} ] ; then
    echo "  successfuly killed"
else
    echo "  fail, port still open"
fi
