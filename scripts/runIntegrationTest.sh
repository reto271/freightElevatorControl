#!/bin/bash

# Change into the script directory
SCRIPTDIR=$(readlink -f $(dirname "$0"))
pushd "${SCRIPTDIR}" > /dev/null
cd ..

rm -rfv Test/integrationTestFeedback
mkdir -p Test/integrationTestFeedback

# Start simulator and store pid (to collect the feedback)
./Simulator/FreightElevatorSimulator.py 2>&1 > Test/integrationTestFeedback/sim.log &
pid_sim=$!

# Delayed start of the DUT. Store pid too.
sleep 1
./ElevatorControl/FreightElevatorController_main.py -s  2>&1 > Test/integrationTestFeedback/dut.log &
pid_dut=$!

# Wait until the simulator finishes and store the feedback
wait ${pid_sim}
feedback_sim=$? #${PIPESTATUS[0]}

# Wait until the DUT finishes and store the feedback
wait ${pid_dut}
feedback_dut=$?

# Output feedback
echo "======================================================"
echo " DUT:"
cat ./Test/integrationTestFeedback/dut.log
echo "======================================================"
echo " Simulator:"
cat ./Test/integrationTestFeedback/sim.log
echo "======================================================"

# Only if both feedbacks are 0, the test was successful
feedback=$(($feedback_sim + $feedback_dut))
echo "feedback sim: ${feedback_sim}"
echo "feedback dut: ${feedback_dut}"
echo "total feedback: ${feedback}"

# Back to the original location
popd > /dev/null

exit ${feedback}
