#!/bin/bash

# Change into the script directory
SCRIPTDIR=$(readlink -f $(dirname "$0"))
pushd "${SCRIPTDIR}" > /dev/null

cd ..
feedback=0

#echo "::: Build ElevatorControl"
#../ElevatorControl/scripts/build.sh "$0"
#feedback=$?

if [ 0 -eq ${feedback} ] ; then
    echo "::: Simulator"
    ./Simulator/scripts/build.sh "$0"
    feedback=$?
fi

if [ 0 -eq ${feedback} ] ; then
    echo "::: Build Test"
    if [ ! -d "../Test/googletest" ] ; then
        echo "Get google test framework"
        ./Test/scripts/getGoogleTest.sh
    fi
    ./Test/scripts/build.sh "$0"
    feedback=$?
fi

if [ 0 -eq ${feedback} ] ; then
    echo "::: Run the Tests"
    ./Test/bin/Test_ElevatorControl --gtest_output=xml:./Test/tmp/gtestresults.xml
    feedback=$?
fi


if [ 0 -eq ${feedback} ] ; then
    echo "::: Run Python Unit-Tests"
    ./ElevatorControl/runTests.py
    feedback=$?
fi

# Back to the original location
popd > /dev/null

exit ${feedback}
